document.getElementById('addCourse').addEventListener('click', function() {
    // Mendapatkan elemen container untuk mata kuliah
    const coursesContainer = document.getElementById('courses');
    // Menghitung jumlah elemen mata kuliah yang sudah ada dan menambahkan 1
    const courseCount = coursesContainer.getElementsByClassName('course').length + 1;

    // Membuat elemen <div> baru untuk satu mata kuliah
    const newCourseDiv = document.createElement('div');
    newCourseDiv.className = 'course';

    // Membuat label untuk input nama mata kuliah
    const newCourseLabel = document.createElement('label');
    newCourseLabel.setAttribute('for', 'course' + courseCount);
    newCourseLabel.textContent = 'Mata Kuliah:';
    newCourseDiv.appendChild(newCourseLabel);

    // Membuat input untuk nama mata kuliah
    const newCourseInput = document.createElement('input');
    newCourseInput.type = 'text';
    newCourseInput.id = 'course' + courseCount;
    newCourseInput.name = 'courses[]';
    newCourseInput.required = true;
    newCourseDiv.appendChild(newCourseInput);

    // Membuat label untuk input nilai mata kuliah
    const newGradeLabel = document.createElement('label');
    newGradeLabel.setAttribute('for', 'grade' + courseCount);
    newGradeLabel.textContent = 'Nilai:';
    newCourseDiv.appendChild(newGradeLabel);

    // Membuat input untuk nilai mata kuliah
    const newGradeInput = document.createElement('input');
    newGradeInput.type = 'number';
    newGradeInput.id = 'grade' + courseCount;
    newGradeInput.name = 'grades[]';
    newGradeInput.min = 0;
    newGradeInput.max = 100;
    newGradeInput.required = true;
    newCourseDiv.appendChild(newGradeInput);

    // Menambahkan elemen mata kuliah baru ke dalam container mata kuliah
    coursesContainer.appendChild(newCourseDiv);
});
