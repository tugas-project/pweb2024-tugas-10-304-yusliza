<?php
// Fungsi untuk mengonversi nilai numerik menjadi nilai huruf
function convertGrade($nilai) {
    // Menggunakan serangkaian kondisional untuk menentukan nilai huruf berdasarkan rentang nilai numerik
    if ($nilai >= 80) return 'A';
    elseif ($nilai >= 76.25) return 'A-';
    elseif ($nilai >= 68.75) return 'B+';
    elseif ($nilai >= 65) return 'B';
    elseif ($nilai >= 62.50) return 'B-';
    elseif ($nilai >= 57.50) return 'C+';
    elseif ($nilai >= 55) return 'C';
    elseif ($nilai >= 51.25) return 'C-';
    elseif ($nilai >= 43.75) return 'D+';
    elseif ($nilai >= 40) return 'D';
    else return 'E';
}

// Mengambil data dari formulir yang disubmit
$nama = $_POST['name'] ?? '';
$nim = $_POST['nim'] ?? '';
$kelas = $_POST['class'] ?? '';
$prodi = $_POST['prodi'] ?? '';
$fakultas = $_POST['faculty'] ?? '';
$mata_kuliah = $_POST['courses'] ?? [];
$nilai = $_POST['grades'] ?? [];

// Menampilkan detail siswa
echo "<h1>Laporan Nilai Siswa</h1>";
echo "Nama: " . htmlspecialchars($nama) . "<br>"; // Menampilkan nama siswa
echo "NIM: " . htmlspecialchars($nim) . "<br>"; // Menampilkan NIM siswa
echo "Kelas: " . htmlspecialchars($kelas) . "<br>"; // Menampilkan kelas siswa
echo "Program Studi: " . htmlspecialchars($prodi) . "<br>"; // Menampilkan program studi siswa
echo "Fakultas: " . htmlspecialchars($fakultas) . "<br>"; // Menampilkan fakultas siswa

// Menampilkan mata kuliah dan nilai siswa
echo "<h2>Mata Kuliah dan Nilai</h2>";
echo "<ul>";
for ($i = 0; $i < count($mata_kuliah); $i++) {
    $mk = htmlspecialchars($mata_kuliah[$i]); // Mengambil dan menyiapkan nama mata kuliah
    $n = htmlspecialchars($nilai[$i]); // Mengambil dan menyiapkan nilai angka
    $nilaiHuruf = convertGrade($n); // Mengonversi nilai angka menjadi nilai huruf
    echo "<li>$mk: Nilai Angka: $n, Nilai Huruf: $nilaiHuruf</li>"; // Menampilkan nama mata kuliah dan nilai (angka dan huruf)
}
echo "</ul>";
?>
