<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Segitiga Sama Sisi</title>
    <link rel="stylesheet"  href="styles.css">
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Generator Segitiga Sama Sisi</h1>
            <p>Masukkan tinggi segitiga untuk menghasilkan pola segitiga yang sempurna</p>
        </div>

        <div class="form-container">
            <!-- Form untuk menerima input tinggi segitiga -->
            <form method="post" action="">
                <label for="tinggi">Masukkan jumlah tinggi segitiga (min 5):</label>
                <input type="number" id="tinggi" name="tinggi" min="5" required>
                <button type="submit">Generate</button>
            </form>
        </div>

        <div class="triangle-container">
            <?php
            // Memeriksa apakah form telah disubmit dan tinggi valid
            if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['tinggi'])) {
                // Mendapatkan dan mengkonversi nilai tinggi dari input form
                $tinggi = intval($_POST['tinggi']);
                // Memastikan tinggi minimal 5
                if ($tinggi >= 5) {
                    echo '<div class="triangle-content"><pre>';
                    // Loop untuk membuat segitiga
                    for ($baris = 1; $baris <= $tinggi; $baris++) {
                        // Loop untuk mencetak spasi di sisi kiri segitiga
                        for ($i = 1; $i <= $tinggi - $baris; $i++) {
                            echo "&nbsp;"; // Gunakan entitas HTML untuk spasi
                        }
                        // Loop untuk mencetak bintang
                        for ($j = 1; $j < 2 * $baris; $j++) {
                            echo "<span class='star'>*</span>";
                        }
                        // Pindah ke baris berikutnya
                        echo "<br>"; // Gunakan <br> untuk baris baru
                    }
                    echo '</pre></div>';
                } else {
                    echo "<p>Tinggi harus lebih dari atau sama dengan 5</p>";
                }
            }
            ?>
        </div>
    </div>
</body>
</html>
